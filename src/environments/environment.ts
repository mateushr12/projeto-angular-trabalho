// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyB3H_94mNjPRUVgEA5cuviyzG90VvQHSsY',
    authDomain: 'appteste-16c7d.firebaseapp.com',
    databaseURL: 'https://appteste-16c7d.firebaseio.com',
    projectId: 'appteste-16c7d',
    storageBucket: 'appteste-16c7d.appspot.com',
    messagingSenderId: '120334451080'
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
