import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AngularFireAuth } from 'angularfire2/auth';

import { LoginFormComponent } from './login-form/login-form.component';
import { LoginComponent } from './login.component';
import { MaterialModule } from '../material/material.module';
import { LoginCadastroFormComponent } from './login-cadastro-form/login-cadastro-form.component';


@NgModule({
  imports: [
    CommonModule, FormsModule, MaterialModule
  ],
  declarations: [LoginFormComponent, LoginComponent, LoginCadastroFormComponent],
  exports: [LoginComponent, LoginFormComponent],
  providers: [ AngularFireAuth ]
})
export class LoginModule { }
