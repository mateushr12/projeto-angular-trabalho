import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-cadastro-form',
  templateUrl: './login-cadastro-form.component.html',
  styleUrls: ['./login-cadastro-form.component.css']
})
export class LoginCadastroFormComponent implements OnInit {

  minhaid: String;

  constructor(private aF: AngularFireDatabase, private afAuth: AngularFireAuth, private rotas: Router) { }

  ngOnInit() {
  }

  form_login_cad(f: NgForm) {
    let email = f.controls.email.value.toString().trim();
    let senha = f.controls.senha.value.toString().trim();

    this.afAuth.auth.createUserWithEmailAndPassword(email, senha)
      .then(t => {
        this.afAuth.authState.subscribe(c => {
          this.cadastrarBanco(f, c.uid);
          alert('Salvo');
          this.rotas.navigate(['/']);
        });
      })
      .catch(c => {
        if (c.code.toString() === 'auth/email-already-in-use') {
          alert('Esse email ja esta cadastrado!');
        } else if (c.code.toString() === 'auth/invalid-email') {
          alert('Formato do email inválido, por favor corriga!');
        } else {
          alert('Algo errado aconteceu, por favor tente novamente!' + c);
        }
      });
  }

  cadastrarBanco(f: NgForm, id: any) {
    this.aF.list('usuarioEmail/' + id.toString()).push({
      nome: f.controls.nome.value
    }
    ).then((t: any) => {
      console.log(t);
    });
  }

  verificaSeSenhasSaoIguais(f: NgForm): boolean {
    if (f.controls.senha) {
      return f.controls.senha.value.toString().trim() === f.controls.csenha.value.toString().trim();
    } else {
      return false;
    }
  }

  verificaFormValido(f: NgForm): boolean {
    return f.form.valid && this.verificaSeSenhasSaoIguais(f);
  }

}
