import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  constructor(private afAuth: AngularFireAuth, private rotas: Router) { }

  ngOnInit() {
  }

  form_login(f: NgForm) {
    if (!f.valid) {
      return;
    }
    this.afAuth.auth.signInAndRetrieveDataWithEmailAndPassword(f.controls.email.value, f.controls.senha.value)
    .then(ok => {
      this.rotas.navigate(['/']);
    });
    f.controls.email.setValue('');
    f.controls.senha.setValue('');
  }

}
