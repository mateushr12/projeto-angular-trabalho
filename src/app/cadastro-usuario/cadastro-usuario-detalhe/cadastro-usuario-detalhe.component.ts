import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularFireDatabase } from 'angularfire2/database';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-cadastro-usuario-detalhe',
  templateUrl: './cadastro-usuario-detalhe.component.html',
  styleUrls: ['./cadastro-usuario-detalhe.component.css']
})
export class CadastroUsuarioDetalheComponent implements OnInit {

  chave: string;
  u: any;



  constructor(private rota: ActivatedRoute, db: AngularFireDatabase) {

    this.chave = this.rota.snapshot.params.id;
    db.object('posts/' + this.chave).valueChanges().subscribe(c => this.u = c);

  }

  ngOnInit() {

  }

}
