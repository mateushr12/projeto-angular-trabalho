import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadastroUsuarioFormComponent } from './cadastro-usuario-form/cadastro-usuario-form.component';
import { CadastroUsuarioListaComponent } from './cadastro-usuario-lista/cadastro-usuario-lista.component';

import { AngularFireDatabase } from 'angularfire2/database';

import { MaterialModule } from './../material/material.module';
import { CadastroUsuarioComponent } from './cadastro-usuario.component';
import { CadastroUsuarioDetalheComponent } from './cadastro-usuario-detalhe/cadastro-usuario-detalhe.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule
  ],
  providers: [AngularFireDatabase],
  exports: [CadastroUsuarioFormComponent, CadastroUsuarioListaComponent, CadastroUsuarioComponent],
  declarations: [CadastroUsuarioFormComponent, CadastroUsuarioListaComponent, CadastroUsuarioComponent, CadastroUsuarioDetalheComponent]
})
export class CadastroUsuarioModule { }
