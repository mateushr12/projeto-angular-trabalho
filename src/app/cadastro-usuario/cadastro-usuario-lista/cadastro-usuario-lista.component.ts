import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-cadastro-usuario-lista',
  templateUrl: './cadastro-usuario-lista.component.html',
  styleUrls: ['./cadastro-usuario-lista.component.css']
})
export class CadastroUsuarioListaComponent implements OnInit {

  u: Array<any>;
  rr: Array<any>;

  lista: AngularFireList<any> = null;

  constructor(db: AngularFireDatabase, auth: AngularFireAuth, private toast: ToastrService, private rotas: Router) {

    // db.list<any>('posts/', ref => ref.orderByKey()).valueChanges().subscribe(c => this.u = c);
    // db.list('posts/', ref => ref.orderByChild('nome').equalTo('hoje')).valueChanges().subscribe(ref => this.ul = ref);

    db.list<any>('posts/').snapshotChanges().pipe(map(act => act.map(a => ({ key: a.payload.key, ...a.payload.val() }))))
      .subscribe(p => this.rr = p);

  }

  ngOnInit() {
  }


  metodo(i: any) {
    console.log(i);
    this.rotas.navigate(['/detalhe', i]);
    // this.toast.error('teste:' + i, 'TESTE 1', { timeOut: 3000});

  }



}
