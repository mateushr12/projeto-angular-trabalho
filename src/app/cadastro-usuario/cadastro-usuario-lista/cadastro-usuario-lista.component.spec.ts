import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastroUsuarioListaComponent } from './cadastro-usuario-lista.component';

describe('CadastroUsuarioListaComponent', () => {
  let component: CadastroUsuarioListaComponent;
  let fixture: ComponentFixture<CadastroUsuarioListaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastroUsuarioListaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastroUsuarioListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
