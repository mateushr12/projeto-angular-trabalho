import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

import { AngularFireDatabase } from 'angularfire2/database';
import { DatePipe } from '@angular/common';
import { AngularFireAuth } from 'angularfire2/auth';
import { auth } from 'firebase';



@Component({
  selector: 'app-cadastro-usuario-form',
  templateUrl: './cadastro-usuario-form.component.html',
  styleUrls: ['./cadastro-usuario-form.component.css']
})
export class CadastroUsuarioFormComponent implements OnInit {

  usuarios: Array<any>;
  datepipe: DatePipe;
  user: any;

  constructor(private angularFire: AngularFireDatabase, autor: AngularFireAuth) {

    // autor.authState.subscribe(aut => {
    //   if (aut) {
    //     this.user = aut.uid;
    //   }
    // });

    autor.authState.subscribe(aut => this.user = aut.uid);

  }

  ngOnInit() { }

  form_submit(f: NgForm) {
    this.angularFire.list('posts').push({
      titulo: f.controls.nome.value,
      recl: f.controls.recl.value,
      suges: f.controls.suges.value,
      cate: f.controls.cate.value,
      data: Date(),
      uid : this.user
    }
    ).then((t: any) => {
      console.log('dadso gravados' + t.key);
      f.controls.nome.setValue('');
      f.controls.recl.setValue('');
      f.controls.suges.setValue('');
      f.controls.cate.setValue('');
    });
  }

}
