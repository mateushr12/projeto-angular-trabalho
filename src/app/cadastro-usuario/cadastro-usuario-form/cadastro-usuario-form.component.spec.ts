import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastroUsuarioFormComponent } from './cadastro-usuario-form.component';

describe('CadastroUsuarioFormComponent', () => {
  let component: CadastroUsuarioFormComponent;
  let fixture: ComponentFixture<CadastroUsuarioFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastroUsuarioFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastroUsuarioFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
