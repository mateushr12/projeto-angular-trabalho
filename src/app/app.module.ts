import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MaterialModule } from './material/material.module';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { AppComponent } from './app.component';

import { AngularFireModule } from 'angularfire2/index';
import { environment } from './../environments/environment';

import { NavbarComponent } from './navbar/navbar.component';
import { CadastroUsuarioModule } from './cadastro-usuario/cadastro-usuario.module';
import { LoginComponent } from './login/login.component';
import { AppRoutingModule } from './app.routing.module';
import { LoginModule } from './login/login.module';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule, BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    HttpModule, CadastroUsuarioModule,
    AngularFireModule.initializeApp(environment.firebase, 'appteste'),
    AppRoutingModule,
    LoginModule,
    ToastrModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
