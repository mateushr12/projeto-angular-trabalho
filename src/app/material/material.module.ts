import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  MatButtonModule,
  MatMenuModule,
  MatToolbarModule,
  MatIconModule,
  MatCardModule,
  MatFormFieldModule, MatSelectModule,
  MatInputModule,
  MatTableModule,
  MatListModule,
  MatTabsModule
} from '@angular/material';

@NgModule({
  imports: [
    MatButtonModule,
    MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatFormFieldModule, MatSelectModule,
    MatInputModule,
    MatTableModule,
    MatListModule,
    MatTabsModule,
    CommonModule
  ],
  exports: [
    MatButtonModule,
    MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatFormFieldModule, MatSelectModule,
    MatTableModule,
    MatListModule,
    MatInputModule,
    MatTabsModule
  ]
})
export class MaterialModule { }
