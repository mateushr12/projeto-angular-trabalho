import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private aut: AngularFireAuth, private rotas: Router) { }

  ngOnInit() {
  }

  logout() {
    this.aut.auth.signOut().then(() => {
      this.rotas.navigate(['/']);
    });
  }

}
