import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginFormComponent } from './login/login-form/login-form.component';
import { CadastroUsuarioComponent } from './cadastro-usuario/cadastro-usuario.component';
import { NgModel } from '@angular/forms';
import { LoginCadastroFormComponent } from './login/login-cadastro-form/login-cadastro-form.component';
import { CadastroUsuarioFormComponent } from './cadastro-usuario/cadastro-usuario-form/cadastro-usuario-form.component';
import { CadastroUsuarioDetalheComponent } from './cadastro-usuario/cadastro-usuario-detalhe/cadastro-usuario-detalhe.component';

const APP_ROUTES: Routes = [
  { path: 'login', component: LoginFormComponent },
  { path: '', component: CadastroUsuarioComponent },
  { path: 'cadastroLogin', component: LoginCadastroFormComponent },
  { path: 'cadastro', component: CadastroUsuarioFormComponent },
  { path: 'detalhe/:id', component: CadastroUsuarioDetalheComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(APP_ROUTES)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
